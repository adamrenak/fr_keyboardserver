/*
 * Copyright (c) 1995, 2008, Oracle and/or its affiliates. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   - Neither the name of Oracle or the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

import java.net.*;
import java.io.*;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;

import javax.swing.JFrame;

public class KnockKnockServer {
  public static void main(String[] args) throws IOException {

    System.out.println(System.getProperty("os.name"));
    System.out.println("hello world \u2202");
    
    int test = Integer.parseInt("00E9", 16);
    
char c = '\u2202';
    
    System.out.println("TEST INTEGER: 00E9 = "+test);
    JFrame f = new JFrame("hello "+(char)233);
    
    f.pack();
    
    f.setVisible(true);
    
    
    ServerSocket serverSocket = null;
    try {
      serverSocket = new ServerSocket(5002);
    } catch (IOException e) {
      System.err.println("Could not listen on port: 4444.");
      System.exit(1);
    }

    Socket clientSocket = null;
    System.out.println("Waiting for accept...");
    try {
      clientSocket = serverSocket.accept();
    } catch (IOException e) {
      System.err.println("Accept failed.");
      System.exit(1);
    }
    System.out.println("SOCKET ACCEPTED");
    PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
    System.out.println("CREATED OUT");
    BufferedReader in = new BufferedReader(new InputStreamReader(
        clientSocket.getInputStream()));
    System.out.println("CREATED IN");
    String inputLine, outputLine;
    KnockKnockProtocol kkp = new KnockKnockProtocol();
//test change
    outputLine = kkp.processInput(null);
    out.println(outputLine);
    while ((inputLine = in.readLine()) != null) {
      System.out.println("Input Line: "+inputLine);
      outputToKeyboard(inputLine);
      outputLine = kkp.processInput(inputLine);
      out.println(outputLine);
      if (outputLine.equals("Bye."))
        break;
    }
    out.close();
    in.close();
    clientSocket.close();
    serverSocket.close();
  }

  public static void outputToKeyboard(String s) {
    System.out.println(s + " START");
    //Integer.toHexString(Integer.parseInt(s) | 0x10000).substring(1)
    char c = (char)(Integer.parseInt(s));
    String forPaste = ""+c;
    try {
      Robot r = new Robot();
      // int i = 0;
      // while (i < 10) {
      // Copy String to Clipboard:
      StringSelection stringSelection = new StringSelection(forPaste);
      Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
      clipboard.setContents(stringSelection, null);
      // Paste Clipboard contents:
      if (System.getProperty("os.name").startsWith("Mac")) {
        r.keyPress(157); // CMD key
        r.keyPress(KeyEvent.VK_V);
        r.keyRelease(157); // CMD key lift
        r.keyRelease(KeyEvent.VK_V);
      } else {
        r.keyPress(KeyEvent.VK_CONTROL);
        r.keyPress(KeyEvent.VK_V);
        r.keyRelease(KeyEvent.VK_CONTROL);
        r.keyRelease(KeyEvent.VK_V);
      }

      // System.out.println(s + " " + i);
      // Thread.sleep(1000L);
      // i++;
      // }
    } catch (AWTException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    System.out.println(s + " END");
  }
}
